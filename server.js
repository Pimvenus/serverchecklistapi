'use strict';

const Hapi = require('hapi');
const Boom = require('boom');
const Pack = require('./package');
const HapiSwagger = require('hapi-swagger');

// Create a server with a host and port
const server = Hapi.server({
    host: 'localhost',
    port: 8000,
    routes: {
        cors: true
    }
});

const swaggerOptions = {
    info: {
        title: 'Check List API Documentation',
        version: Pack.version,
    },
};

const dbOpts = {
    url: 'mongodb://localhost:27017/checklistdb',
    settings: {
        poolSize: 10
    },
    decorate: true
};

// Add the route




server.route({ // GET content 
    method: 'GET',
    path: '/student/{email}/{password}', //path'
    config: {
        auth: false,
        description: 'Get student by email and password',
        tags: ['api']
    },
    handler: async(req, reply) => {
        try {
            const db = req.mongo.db;
            const res = await db.collection('students').find().toArray(); //ชื่อตาราง
            var usr_data;

            for (let i = 0; i < res.length; i++) {
                if (res[i].email == req.params.email && res[i].password == req.params.password)
                    usr_data = res[i]; //เอาผลลัพธ์จากการเช็คเก็บใส่ตัวแปรชื่อ usr_data

            }

            return {
                data: usr_data,
                message: 'OK',
                statusCode: 200,
            };

        } catch (error) {
            return (Boom.badGateway(error));
        }
    }
});







// Start the server
const init = async() => {
    await server.register([{
            plugin: require('hapi-mongodb'),
            options: dbOpts
        },
        require('inert'),
        require('vision'),
        {
            plugin: HapiSwagger,
            options: swaggerOptions
        }
    ]);
    await server.start();
    console.log(`Server started at ${server.info.uri}`);
};

process.on('unhandledRejection', (err) => {
    console.error(err);
    process.exit(1);
})

init();